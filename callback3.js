const fs = require("fs");

function getCardsByListId(filePath, listId, callbackFunction) {

    if (filePath && listId && callbackFunction) {
        if (typeof callbackFunction !== 'function') {
            console.error("callback is not a function");
        } else if (typeof listId !== 'string') {
            callbackFunction("Board id is not a string");
        } else if (typeof filePath !== 'string') {
            callbackFunction("File path is not a string");
        } else {

            fs.readFile(filePath, 'utf-8', (error, data) => {
                if (error) {
                    callbackFunction(error);
                } else {
                    try {
                        let cards = JSON.parse(data);
                        let checkCards = true;
                        if (typeof (cards) === 'object') {
                            for (let id in cards) {
                                let cardArray = cards[id];
                                if (Array.isArray(cardArray)) {
                                    cardArray.forEach((card) => {
                                        if (!card.id || !card.description) {
                                            checkCards = false;
                                        }
                                    })
                                } else {
                                    checkCards = false;
                                }
                            }
                        } else {
                            checkCards = false;
                        }

                        if (checkCards) {
                            setTimeout(() => {

                                if (cards[listId]) {
                                    for (let id in cards) {
                                        if (id === listId) {
                                            callbackFunction(null, cards[id]);
                                            return cards[id];
                                        }
                                    }
                                } else {
                                    callbackFunction("--Id(" + listId + ") is not found in cards--");
                                }

                            }, 2000);
                        } else {
                            callbackFunction("--The file does not have the required json data--");
                        }
                    } catch (error) {
                        callbackFunction(error);
                    }
                }
            })

        }

    } else {

        if (!listId && !callbackFunction && !filePath) {
            console.log("--File Path, List id and callback function is undefined--");
        }
        if (!listId) {
            console.log("--List id is undefined--");
        }
        if (!callbackFunction) {
            console.log("--Callback function is undefined--");
        }
        if (!filePath) {
            console.log("--File path is undefined--");
        }

    }

}

module.exports = getCardsByListId;