const fs = require("fs");

function getListsByBoardId(filePath, boardId, callbackFunction) {

    if (filePath && boardId && callbackFunction) {
        if (typeof callbackFunction !== 'function') {
            console.error("callback is not a function");
        } else if (typeof boardId !== 'string') {
            callbackFunction("Board id is not a string");
        } else if (typeof filePath !== 'string') {
            callbackFunction("File path is not a string");
        } else {

            fs.readFile(filePath, 'utf-8', (error, data) => {
                if (error) {
                    callbackFunction(error);
                } else {
                    try {
                        let lists = JSON.parse(data);
                        let checkLists = true;
                        if (typeof (lists) === 'object') {
                            for (let board in lists) {
                                let listArray = lists[board];
                                if (Array.isArray(listArray)) {
                                    listArray.forEach((board) => {
                                        if (!board.id || !board.name) {
                                            checkLists = false;
                                        }
                                    })
                                } else {
                                    checkLists = false;
                                }
                            }
                        } else {
                            checkLists = false;
                        }

                        if (checkLists) {
                            setTimeout(() => {
                                if (lists[boardId]) {
                                    for (let id in lists) {
                                        if (id === boardId) {
                                            callbackFunction(null, lists[id]);
                                            return lists[id];
                                        }
                                    }

                                } else {

                                    callbackFunction("--Board id is not found in the lists--");

                                }
                            }, 2000);
                        } else {
                            callbackFunction("--The file does not have the required json data--");
                        }
                    } catch (error) {
                        callbackFunction(error);
                    }

                }
            })
        }


    } else {

        if (!filePath && !boardId && !callbackFunction) {
            console.log("--File path, Board id and callback function are undefined--");
        } else if (!boardId) {
            console.log("--Board id is undefined--");
        } else if (!callbackFunction) {
            console.log("--Callback function is undefined--");
        } else {
            console.log("--File path is undefined--");
        }

    }

}

module.exports = getListsByBoardId;