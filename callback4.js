const getBoardsInformationById = require("./callback1");
const getListsByBoardId = require("./callback2");
const getCardsByListId = require("./callback3");
const fs = require("fs");

function getInformation(filePath, callbackFunction) {
    if (!callbackFunction || typeof callbackFunction !== 'function') {
        console.error("Callback function is not defined properly");
    }
    else if (!filePath || typeof filePath !== 'string') {
        callbackFunction("File path is not defined properly");
    } else {
        fs.readFile(filePath, 'utf-8', (error, data) => {
            if (error) {
                callbackFunction(error);
            } else {
                try {
                    setTimeout(() => {
                        let boards = JSON.parse(data);
                        let boardId;
                        for (let index in boards) {
                            if (boards[index].name === "Thanos") {
                                boardId = boards[index].id;
                            }
                        }

                        if (!boardId) {
                            console.log("Board id is undefined");
                        } else {

                            getBoardsInformationById("./boards.json", boardId, (error, board) => {
                                if (error) {
                                    callbackFunction(error);
                                } else {
                                    console.log(board);
                                    getListsByBoardId("./lists.json", boardId, (err, lists) => {
                                        if (err) {
                                            callbackFunction(err);
                                        } else {
                                            console.log(lists);
                                            lists.forEach((list) => {
                                                if (list.name === "Mind" || list.name === "Space") {
                                                    getCardsByListId("./cards.json", list['id'], (error, cards) => {
                                                        if (error) {
                                                            callbackFunction(error);
                                                        } else {
                                                            console.log(cards)
                                                        }
                                                    })
                                                }
                                            })
                                        }

                                    });
                                }

                            });

                        }

                    }, 2000);
                } catch (error) {
                    callbackFunction(error);
                }
            }
        })
    }

}

module.exports = getInformation;