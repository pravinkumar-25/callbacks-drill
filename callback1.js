const fs = require("fs");

function getBoardsInformationById(filePath, boardId, callbackFunction) {

    if (filePath && boardId && callbackFunction) {
        if (typeof callbackFunction !== 'function') {
            console.error("callback is not a function");
        } else if (typeof boardId !== 'string') {
            callbackFunction("Board id is not a string");
        } else if (typeof filePath !== 'string') {
            callbackFunction("File path is not a string");
        } else {

            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    callbackFunction(err);
                } else {
                    try {
                        let checkBoard = true;
                        let boards = JSON.parse(data);
                        if (Array.isArray(boards)) {
                            boards.forEach(board => {
                                if (!board.id || !board.name) {
                                    checkBoard = false;
                                }
                            });
                        } else {
                            checkBoard = false;
                        }

                        if (checkBoard) {
                            setTimeout(() => {

                                let checkId = boards.some((board) => board.id === boardId)
                                if (checkId) {
                                    boards.forEach((board) => {
                                        if (board.id === boardId) {
                                            callbackFunction(null, board);
                                            return board;
                                        }
                                    })
                                } else {
                                    callbackFunction("--Id is not found in boards--");
                                }

                            }, 2000)
                        } else {
                            callbackFunction("--The file does not have the required json data--");
                        }
                    } catch (error) {
                        callbackFunction(error);
                    }

                }

            })
        }

    } else {

        if (!boardId && !callbackFunction && !filePath) {
            console.log("--Path of the json file, callback function and board id are undefined--");
        } else if (!boardId) {
            console.log("--Id is undefined--");
        } else if (!callbackFunction) {
            console.log("--Callback function is undefined--");
        } else {
            console.log("--Path of the json file is undefined--")
        }

    }
}


module.exports = getBoardsInformationById;