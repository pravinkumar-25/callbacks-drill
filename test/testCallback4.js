const getData = require("../callback4");


//verifying that the code works correctly
getData("./boards.json",(error,data)=>{
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when callback is undefined
getData("./boards.json");

//verifying that it does not work, when path or file name is wrong
getData("./boardds.json",(error,data)=>{
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when the file has bad json data
getData("./boards-corrupted.json",(error,data)=>{
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});