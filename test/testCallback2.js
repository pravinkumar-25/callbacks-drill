const listOfBoard = require("../callback2");

//verifying whether the code works
listOfBoard("./lists.json", 'mcu453ed', (error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
})

//verifying that it does not work, when id is wrong
listOfBoard("./lists.json", 'mcu453ed111', (error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
})

//verifying that it does not work, when path or file name is wrong
listOfBoard("./listsggh.json", 'mcu453ed', (error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
})

//verifying that it does not work, when callback function is undefined
listOfBoard("./lists.json", 'mcu453ed')

//verifying that it does not work, when both callback and id are undefined
listOfBoard("./lists.json")

//verifying that it does not work, when no parameter are passed into the function
listOfBoard()

//verifying that it does not work, when the file has bad json data
listOfBoard("./boards-corrupted.json", 'mcu453ed', (error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
})