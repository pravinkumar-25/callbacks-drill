const getCardsByListId = require("../callback3");

//verifying whether the code works
getCardsByListId("./cards.json","jwkh245",(error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when id is wrong
getCardsByListId("./cards.json","jwkh2451ds",(error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when path or file name is wrong
getCardsByListId("./cardsWrong.json","jwkh245");

//verifying that it does not work, when both callback and id are undefined
getCardsByListId("./cards.json");

//verifying that it does not work, when no parameter are passed into the function
getCardsByListId();

//verifying that it does not work, when the file has bad json data
getCardsByListId("./boards-corrupted.json","jwkh245",(error,data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});