const boardInformation = require("../callback1");

//verifying whether the code works
boardInformation("./boards.json", "mcu453ed", (error, data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when id is wrong
boardInformation("./boards.json", "mcu453ed111", (error, data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when path or file name is wrong
boardInformation("./boardsadsfas.json", "mcu453ed", (error, data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});

//verifying that it does not work, when callback function is undefined
boardInformation("./boards.json", "mcu453ed");

//verifying that it does not work, when both callback and id are undefined
boardInformation("./boards.json");

//verifying that it does not work, when no parameter are passed into the function
boardInformation();

//verifying that it does not work, when the file has bad json data
boardInformation("./boards-corrupted.json", "mcu453ed", (error, data) => {
    if (error) {
        console.log("Failure");
        console.error(error);
    } else {
        console.log("Success");
        console.log(data)
    }
});